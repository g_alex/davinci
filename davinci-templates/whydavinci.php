<div class="why-davinci">
	<div class="content">
		<h2 class="block-title">DE CE LICEUL DA VINCI?</h2>

		<ul>
			<li>Deoarece venim cu o nouă viziune – educație bazată pe relație!</li>
			<li>Deoarece oferim condiții moderne de instruire (cabinete, laboratoare dotate cu echipament ultramodern)!</li>
			<li>Deoarece avem o echipă de profesioniști în domeniul educației!</li>
			<li>Deoarece suntem amplasațI comod, chiar în centrul chișinăului!</li>
			<li>Deoarece oferim o gamă largă de facilitățI:</li>
		</ul>
	</div>	
	<div class="image">
		<img src="<?php echo get_stylesheet_directory_uri().'/img/s4.jpg'; ?>" alt="Liceul Da Vinci"/>
	</div>

</div>