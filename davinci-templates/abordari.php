<div class="abordari">
	<div class="upper-part">
		<div class="left-side">
			<img src="<?php echo get_stylesheet_directory_uri().'/img/s2.jpg'; ?>" alt="Liceul Da Vinci"/>

			<h2 class="block-title">
				ABORDĂRI EDUCAȚIONALE <br/> INEDITE
			</h2>
		</div>
		<div class="right-side">
			<img src="<?php echo get_stylesheet_directory_uri().'/img/s3.jpg'; ?>" alt="Liceul Da Vinci"/>
		</div>
	</div>

	<div class="bottom-part">
		<div class="left-side">
			<ul>
				<li>Accent primordial pe elaborarea proiectelor individuale și de grup, inclusiv: explorare, cercetare, prezentare publică.</li>
				<li>Maximizarea eficienței educației în timpul programului școlar</li>
				<li>Renunțarea la teme pentru acasă </li>
				<li>Program de week-end (opțional)</li>
			</ul>
		</div>

		<div class="right-side">
			<ul>
				<li>Ajustarea demersului educațional la particularită țile psihologice, fiziologice și fizice ale copiilor.</li>
				<li>Programe educaționale de incluziune</li>
				<li>Program prelungit (la solicitare)</li>
			</ul>
		</div>
	</div>
</div>

