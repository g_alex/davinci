<div class="curiculum">
	<h2 class="block-title">CURRICULUM ȘI EVALUARE</h2>

	<div class="curiculum-content">
		<div class="col col1">
			<p>Procesul de instruire se va desfășură în conformitate cu Planul-cadru, standardele educaționale și Curriculum-ul Național aprobat de Ministerul Educației, Culturii și Cercetării al Republicii Moldova. </p>

			<p>În cel mai scurt timp, instituția intenționează să obțină, acreditarea pentru implementarea programelor educaționale oferite de Cambridge International Education (Cambridge Primary, Cambridge Secondary, Cambridge Advanced) și International Baccalaureate Organisation sau IB (Primary Years Programme, Middle Years Programme, Diploma Programme).</p>
		</div>

		<div class="col col2">
			<p>Astfel, absolvenții instituției vor avea posibilitate de a obține, la solicitare, Certificate și Diplome de studii internațional recunoscute, în afară de actele naționale de studii.</p>

			<p>Evaluarea succesului școlar va fi organizat conform unui Regulament inedit, parte componentă a Sistemului Instituționalizat de Evaluare, bazat pe măsurarea indicilor de progres (cognitivi, praxiologici și afectivi) ai elevilor.</p>

		</div>
	</div>
</div>