<div class="beneficii">
	<div class="beneficii-header" style="background-image: url(<?php echo get_stylesheet_directory_uri().'/img/s5-dark.jpg'; ?>)">
		<h2 class="block-title">BENEFICII</h2>
	</div>
	<div class="beneficii-content">
		<div class="box box-dark">
			<div class="inner">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/ico-lang.png'; ?>" atl="Studierea limbilor" />
				Studierea limbilor <br/> Engleză şi Germană
			</div>
		</div>

		<div class="box">
			<div class="inner">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/ico-tech.png'; ?>" atl="Tehnologii de Ultimă Generaţie" />
				Tehnologii de Ultimă Generaţie
			</div>
		</div>

		<div class="box box-dark">
			<div class="inner">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/ico-cantina.png'; ?>" atl="Cantină Proprie" />
				Cantină Proprie
			</div>
		</div>

		<div class="box">
			<div class="inner">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/ico-security.png'; ?>" atl="Securitate și Monitorizare" />
				Securitate și Monitorizare
			</div>
		</div>

		<div class="box box-dark">
			<div class="inner">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/ico-basin.png'; ?>" atl="Teren Sportiv, Bazin de înot" />
				Teren Sportiv, Bazin de înot
			</div>
		</div>

		<div class="box">
			<div class="inner">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/ico-med.png'; ?>" atl="Servicii de asistență individuală" />
				Servicii de asistență individuală <br/>
				<span>(Help-Desk, Medicală, Psihologică, <br/> de Incluziune)</span>
			</div>
		</div>
		
	</div>
</div>