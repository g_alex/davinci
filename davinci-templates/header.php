<div class="header">
	<div class="logo">
		<img src="<?php echo get_stylesheet_directory_uri().'/img/logo.png'; ?>" alt="Liceul Da Vinci"/>
	</div>
	<div class="contacts">
		<div class="contacts-address">
			<i class="fas fa-map-marker-alt"></i>
			Str. Constantin Stamati 10
		</div>
		<div class="contacts-phone">
			<a href="tel:069138924">
				<i class="fas fa-phone"></i>
				069138924
			</a>
		</div>

	</div>
</div>