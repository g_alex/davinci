<div class="intro">
	<div class="content-text">
		<h2 class="block-title">LICEUL PRIVAT <b>„DA VINCI”</b> ESTE MAI MULT DECÂT O ŞCOALĂ.</h2>

		<div class="sub-title">
			AICI POŢI GĂSI TOT DE CE AI NEVOIE PENTRU A CREŞTE O PERSONALITATE ADAPTATĂ LA DIVERSITATEA CULTURALĂ GLOBALĂ.
		</div>

		<ol>
			<li data-link="abordari">ABORDĂRI EDUCAȚIONALE INEDITE</li>
			<li data-link="why-davinci">DE CE LICEUL DA VINCI?</li>
			<li data-link="curiculum">CURRICULUM ȘI EVALUARE</li>
			<li data-link="activitati">ACTIVITĂȚI EXTRACURRICULARE</li>
		</ol>
	</div>
	<div class="content-img">
		<img src="<?php echo get_stylesheet_directory_uri().'/img/s1.jpg'; ?>" alt="Liceul Da Vinci"/>
		<div class="dark-bg"></div>
	</div>

</div>
<div class="scroll-down">
	Află mai multe <i class="fas fa-angle-down"></i>
</div>