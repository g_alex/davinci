<div class="activitati">
	<h2 class="block-title">
		ACTIVITĂȚI EXTRACURRICULARE. <br>
		IMPLICARE COMUNITARĂ
	</h2>

	<div class="activitati-content">
		<div class="activitati-block">
			<div class="img">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/ico-club.png'; ?>" atl="Cluburi pe interese" />
			</div>

			<div class="activitati-text">
				<h4>Cluburi pe interese:</h4>

				<p>Robotică și Tehnologii informaționale</p>
				<p>Investigații de laborator</p>
				<p>Extensii curriculare specializate </p>
				<p>Șah, jocuri intelectuale</p>
				<p>Debate</p>

				<div class="border-line"></div>
			</div>
			
		</div>

		<div class="activitati-block">
			<div class="img">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/ico-music.png'; ?>" atl="Cluburi pe interese" />
			</div>

			<div class="activitati-text">
				<h4>Muzică și Arte:</h4>

				<p>Pian, chitară, vioară, tobe</p>
				<p>Fluier și trompetă</p>
				<p>Xilofon </p>
				<p>Coregrafie şi Street Dance</p>
				<p>Artă dramatică</p>
				<p>Pictură și ceramică</p>

				<div class="border-line"></div>
			</div>
			
		</div>

		<div class="activitati-block">
			<div class="img">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/ico-sports.png'; ?>" atl="Cluburi pe interese" />
			</div>

			<div class="activitati-text">
				<h4>Activități sportive:</h4>

				<p>Înot, Polo pe apă,</p>
				<p>Volei, Baschet</p>
				<p>Squash, Karate</p>
				<p>Tenis de masă, tenis de câmp</p>

			</div>
		</div>

		<div class="activitati-block">
			<div class="img">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/ico-comunity.png'; ?>" atl="Cluburi pe interese" />
			</div>

			<div class="activitati-text">
				<h4>Implicare comunitară:</h4>

				<p>Voluntariat comunitar</p>
				<p>Mentorat, tutorat</p>

			</div>
		</div>


	</div>
</div>