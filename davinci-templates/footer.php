<div class="footer">
	<div class="social-media flex">
		<span class="label">Urmăreşte-ne pe:</span>
		<div class="info">
			<a href="https://www.facebook.com/davinci.moldova/" target="_blank">
				<i class="fab fa-facebook"></i>
				Facebook
			</a>
		</div>
		<!-- <div class="info">
			<a href="https://www.facebook.com/davinci.moldova/" target="_blank">
				<i class="fab fa-instagram"></i>
				Instagram
			</a>
		</div> -->
	</div>
	<div class="call flex">
		<span class="label">Pentru Informații:</span>
		<div class="info">
			<a href="tel:069138924">
				<i class="fas fa-phone"></i>
				069138924
			</a>
		</div>
	</div>
</div>