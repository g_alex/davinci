<div class="map">
	<div id="map" class="map-preview">

	</div>
	<script>
      function initMap() {
        var uluru = {lat: 47.033663, lng: 28.833855};

         var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 17,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA85Aw00JB42NaERpB-iNHzVMc74JvGvcM&callback=initMap">
    </script>

	<div class="description">
		<h2 class="block-title">
			Our Location

			<div class="map-ico">
				<i class="fas fa-map-marker-alt"></i>
				<i class="far fa-map"></i>
			</div>
		</h2>
		Republica Moldova<br/>
		MD-2024, Chişinău,<br/>
		Str. Constantin Stamati 10.<br/>
		TEL: 069138924</p>
	</div>
</div>