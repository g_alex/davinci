<?php
/**
 * Template Name: Blank Page Template
 *
 * Template for displaying a blank page.
 *
 * @package understrap
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title"
		content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>
<body class="davinci-home">
	<?php 
		get_template_part( 'davinci-templates/header');

		get_template_part( 'davinci-templates/intro');

		get_template_part( 'davinci-templates/abordari');

		get_template_part( 'davinci-templates/map');

		get_template_part( 'davinci-templates/whydavinci');

		get_template_part( 'davinci-templates/beneficii');
		
		get_template_part( 'davinci-templates/curiculum');

		get_template_part( 'davinci-templates/activitati');

		get_template_part( 'davinci-templates/footer');
		
	?>
<?php wp_footer(); ?>
</body>
</html>
